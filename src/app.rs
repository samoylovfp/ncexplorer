use super::{
    file::{load_data_into_texture, load_file, OpenNetcdfFile},
    layer::{Layer, LayerView},
    utils::{toggle, RerenderWatcher},
};
use eframe::{
    egui::{self, plot::Plot, Pos2, Rect}, App, Frame,
};
use itertools::Itertools;
use poll_promise::Promise;
use rayon::spawn;
use std::{path::PathBuf, sync::Arc, task::Poll};
use tracing::*;

/// Representation of the application state.
pub struct NcExplorerApp {
    files: Vec<OpenNetcdfFile>,
    drawn_layers: Vec<Layer>,
    is_files_open: bool,
    is_layers_open: bool,
    plot_bounds: Rect,
    rerender_watcher: RerenderWatcher,
}

impl NcExplorerApp {
    pub fn new() -> Self {
        Self {
            files: Vec::new(),
            is_files_open: true,
            drawn_layers: vec![],
            is_layers_open: false,
            plot_bounds: Rect::NOTHING,
            rerender_watcher: RerenderWatcher::new(),
        }
    }

    pub fn load_file_bg(&mut self, path: PathBuf) {
        if self.files.iter().any(|f| f.path == path) {
            println!("File {path:?} already open");
            return;
        }
        let (sender, promise) = Promise::new();
        let path_clone = path.clone();
        spawn(|| sender.send(load_file(path_clone)));
        self.files.push(OpenNetcdfFile {
            path,
            loaded_file: promise,
        });
    }

    pub fn render_left_panel(&mut self, context: &egui::Context, ui: &mut egui::Ui) {
        if self.files.is_empty() {
            ui.label("Drop files here");
        }
        let mut files_to_close = vec![];
        let mut layers_to_draw = vec![];
        for f in &self.files {
            f.render_ui(ui, &mut layers_to_draw, &mut files_to_close);
        }
        self.files.retain(|f| !files_to_close.contains(&f.path));

        layers_to_draw
            .iter_mut()
            .for_each(|l| Self::load_view(l, l.default_view(), context));
        self.drawn_layers.extend(layers_to_draw);
    }

    fn render_central_panel(&mut self, context: &egui::Context) {
        let layer_values = self
            .drawn_layers
            .iter()
            .filter_map(|l| {
                if let Some(lv) = &l.current_view {
                    if let Some(ld) = &lv.loaded_data {
                        if let Poll::Ready(Ok(data)) = ld.poll() {
                            return Some((
                                l.variable_name.clone(),
                                data.bounds,
                                Arc::clone(&data.values),
                            ));
                        }
                        return None;
                    }
                }
                None
            })
            .collect_vec();

        egui::CentralPanel::default().show(context, |ui| {
            Plot::new("plot")
                .data_aspect(1.0)
                .label_formatter(move |_name, value| {
                    let layer_label = layer_values
                        .iter()
                        .filter_map(|(var_name, bounds, data)| {
                            if bounds.contains(value.to_pos2()) {
                                let x_idx = (value.x - bounds.min.x as f64) / bounds.width() as f64
                                    * data.row_size as f64;
                                let y_idx = (bounds.max.y as f64 - value.y)
                                    / bounds.height() as f64
                                    * data.col_size as f64;
                                let actual_value = data
                                    .data
                                    .get(y_idx as usize * data.row_size + x_idx as usize);
                                if let Some(v) = actual_value {
                                    return Some(format!("{var_name}={v}"));
                                }
                            }
                            None
                        })
                        .join("\n");
                    format!("lat={:.3}\nlon={:.3}\n{layer_label}", value.y, value.x)
                })
                .show(ui, |plot_ui| {
                    let plot_bounds = plot_ui.plot_bounds();
                    let [minx, miny] = plot_bounds.min();
                    let [maxx, maxy] = plot_bounds.max();
                    self.plot_bounds = Rect {
                        min: Pos2::new(minx as f32, miny as f32),
                        max: Pos2::new(maxx as f32, maxy as f32),
                    };
                    for layer in &self.drawn_layers {
                        if layer.visible {
                            layer.draw(plot_ui);
                        }
                    }
                });
            if self.rerender_watcher.trigger(self.plot_bounds) {
                debug!("Re-rendering with plot bounds {:?}", self.plot_bounds);
                for layer in &mut self.drawn_layers {
                    Self::load_view(
                        layer,
                        LayerView {
                            loaded_data: None,
                            rect: self.plot_bounds,
                        },
                        context,
                    )
                }
            }
        });
    }

    fn load_dropped_files(&mut self, ctx: &egui::Context) {
        ctx.input()
            .raw
            .dropped_files
            .iter()
            .inspect(|df| {
                dbg!(df);
            })
            .filter_map(|f| f.path.clone())
            .for_each(|df| self.load_file_bg(df));
    }

    fn load_view(l: &mut Layer, mut view: LayerView, context: &egui::Context) {
        let (sender, promise) = Promise::new();
        let cloned_file = Arc::clone(&l.file);
        let var_name = l.variable_name.clone();
        let context_clone = context.clone();
        let bounds = view.rect;
        spawn(move || {
            let v = var_name;
            sender.send(load_data_into_texture(
                cloned_file,
                &v,
                bounds,
                context_clone,
            ))
        });
        view.loaded_data = Some(promise);
        l.next_view = Some(view);
    }

    fn render_right_panel(&mut self, ui: &mut egui::Ui) {
        let mut layers_to_drop = vec![];
        for (i, layer) in self.drawn_layers.iter_mut().enumerate() {
            ui.horizontal(|ui| {
                ui.label(&layer.variable_name);
                ui.checkbox(&mut layer.visible, "vis");
                if ui.button("x").clicked() {
                    layers_to_drop.push(i);
                }
            });
        }
        for &i in layers_to_drop.iter().rev() {
            self.drawn_layers.remove(i);
        }
    }
}

impl App for NcExplorerApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut Frame) {
        self.load_dropped_files(ctx);
        for l in &mut self.drawn_layers {
            l.check_if_loaded();
        }

        egui::TopBottomPanel::top("tools").show(ctx, |ui| {
            ui.horizontal(|ui| {
                toggle("Files", ui, &mut self.is_files_open);
                toggle("Layers", ui, &mut self.is_layers_open);
            });
        });
        if self.is_files_open {
            egui::SidePanel::left("files")
                .max_width(1.0)
                .show(ctx, |ui| {
                    self.render_left_panel(ctx, ui);
                });
        }
        if self.is_layers_open {
            egui::SidePanel::right("nav").show(ctx, |ui| {
                self.render_right_panel(ui);
            });
        }
        self.render_central_panel(ctx);
        // to re-paint when a background operation is done
        ctx.request_repaint();
    }

}
