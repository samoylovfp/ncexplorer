use std::time::{Duration, Instant};

use eframe::egui::{self, Color32, Rect, Ui, Widget};

pub fn toggle(text: &str, ui: &mut Ui, val: &mut bool) {
    if egui::Button::new(text)
        .fill(if *val { Color32::BLUE } else { Color32::BLACK })
        .ui(ui)
        .clicked()
    {
        *val = !*val;
    }
}

pub struct RerenderWatcher {
    triggered: Rect,
    viewed: Rect,
    changed: Instant,
}

const RERENDER_TRIGGER: Duration = Duration::from_millis(300);

impl RerenderWatcher {
    pub fn new() -> RerenderWatcher {
        RerenderWatcher {
            triggered: Rect::NOTHING,
            viewed: Rect::NOTHING,
            changed: Instant::now(),
        }
    }

    pub fn trigger(&mut self, bounds: Rect) -> bool {
        if bounds == self.triggered {
            return false;
        }
        if bounds != self.viewed {
            self.viewed = bounds;
            self.changed = Instant::now();
            false
        } else if self.changed.elapsed() >= RERENDER_TRIGGER {
            self.triggered = bounds;
            self.changed = Instant::now();
            true
        } else {
            false
        }
    }
}
